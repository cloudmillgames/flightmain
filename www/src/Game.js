/* global Phaser */
/* global Debugsy */
/* global game */
/* global Startup */
/* global StartGame */
/* global MainMenu */
/* global Gameplay */
game.state.add('Startup', Startup, false);
game.state.add('StartGame', StartGame, false);
game.state.add('MainMenu', MainMenu, false);
game.state.add('Gameplay', Gameplay, false);
game.state.start("Startup", false);
