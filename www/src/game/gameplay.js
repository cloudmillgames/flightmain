// Class Gameplay

/* global debugsy */
/* global DEBUG */
/* global Utils */
/* global Phaser */
/* global game */
/* global gamesouls */

var Gameplay = function() {
};

Gameplay.prototype = {
    init: function() {
        // Physics
        game.physics.startSystem(Phaser.Physics.ARCADE);
        game.physics.arcade.gravity.y = 0;
        
        // Background
        this.bgLayer1 = [];
        this.bgLayer1Tweens = [];
        this.bgLayer2 = [];
        this.bgLayer2Tweens = [];
        this.bgLayer3 = [];
        this.bgLayer3Tweens = [];
        this.BG1_SCROLL_DURATION = 45 * 1000;
        this.BG2_SCROLL_DURATION = 60 * 1000;
        this.BG3_SCROLL_DURATION = 75 * 1000;
        
        // Player
        this.player = null;
        
        // UI
        this.stageText = null;
        this.stageTextTween = null;
    },
    
    preload: function() {
        
    },
    
    create: function() {
        // Background
        var sf = 2;
        this.bgLayer1.push(game.add.image(0, 0, 'bg_layer_1', null, gamesouls.bgLayer));
        this.bgLayer1.push(game.add.image(gamesouls.width, 0, 'bg_layer_1', null, gamesouls.bgLayer));
        this.bgLayer1[0].scale.x = sf; this.bgLayer1[0].scale.y = sf;
        this.bgLayer1[1].scale.x = sf; this.bgLayer1[1].scale.y = sf;
        //
        this.bgLayer2.push(game.add.image(0, 0, 'bg_layer_2', null, gamesouls.bgLayer));
        this.bgLayer2.push(game.add.image(gamesouls.width, 0, 'bg_layer_2', null, gamesouls.bgLayer));
        this.bgLayer2[0].scale.x = sf; this.bgLayer2[0].scale.y = sf;
        this.bgLayer2[1].scale.x = sf; this.bgLayer2[1].scale.y = sf;
        //
        this.bgLayer3.push(game.add.image(0, 0, 'bg_layer_3', null, gamesouls.bgLayer));
        this.bgLayer3.push(game.add.image(gamesouls.width, 0, 'bg_layer_3', null, gamesouls.bgLayer));
        this.bgLayer3[0].scale.x = sf; this.bgLayer3[0].scale.y = sf;
        this.bgLayer3[1].scale.x = sf; this.bgLayer3[1].scale.y = sf;
        
        // Player
        this.player = new Player();
        this.player.create();
        
        // Scroll away!
        for(var i = 0; i < 2; ++i) {
            this.bgLayer1Tweens.push(game.add.tween(this.bgLayer1[i]));
            this.scrollBG(this.bgLayer1[i], this.bgLayer1Tweens[i], this.BG1_SCROLL_DURATION);
            this.bgLayer1Tweens[i].repeat(-1);
            this.bgLayer1Tweens[i].onLoop.add(this.bgLayerScrollComplete, this, 0);
            
            this.bgLayer2Tweens.push(game.add.tween(this.bgLayer2[i]));
            this.scrollBG(this.bgLayer2[i], this.bgLayer2Tweens[i], this.BG2_SCROLL_DURATION);
            this.bgLayer2Tweens[i].repeat(-1);
            this.bgLayer2Tweens[i].onLoop.add(this.bgLayerScrollComplete, this, 0);
            
            this.bgLayer3Tweens.push(game.add.tween(this.bgLayer3[i]));
            this.scrollBG(this.bgLayer3[i], this.bgLayer3Tweens[i], this.BG3_SCROLL_DURATION);
            this.bgLayer3Tweens[i].repeat(-1);
            this.bgLayer3Tweens[i].onLoop.add(this.bgLayerScrollComplete, this, 0);
            
        }
        
        // UI
        this.stageText = game.add.text(gamesouls.centerX, gamesouls.centerY, "STAGE 1", null, gamesouls.overLayer);
        this.stageText.anchor.setTo(0.5);
        this.stageText.font = 'M20_SP-RANKER';
        this.stageText.fontSize = 50;
        this.stageText.align = 'center';
        this.stageText.fill = '#fff';

        this.stageText.scale.x = 0.0;
        this.stageText.scale.y = 0.0;
        game.add.tween(this.stageText.scale).to({x:1, y:1}, 500, Phaser.Easing.Cubic.In, true, 500);
        game.add.tween(this.stageText).to({alpha:0}, 250, Phaser.Easing.Linear.None, true, 2000).onComplete.add(
            function(){this.stageText.destroy();}, this);
        
        // Debugsy
        debugsy.create();
        debugsy.log("Gameplay");
        
        gamesouls.resetInput();
    },
    
    update: function() {
        gamesouls.update();
        this.player.update();
    },
    
    render: function() {
        if(DEBUG) {
            debugsy.render();
            /*gamesouls.drawDebug();
            game.debug.inputInfo(32, 32);
            game.debug.pointer(game.input.activePointer);
            game.debug.text("Client X and Y: " + game.input.activePointer.x + ", " + game.input.activePointer.y, 32, 400, "#fff");*/
        }
    },
    
    scrollBG: function(_layer, _tween, _duration) {
        _tween.to({x: _layer.x - gamesouls.width}, _duration, null, true, 0, true);
    },
    
    bgLayerScrollComplete: function(_layer, _tween) {
        if(_layer.x <= -gamesouls.width) {
            _layer.x = gamesouls.width; 
        }
    },
};
