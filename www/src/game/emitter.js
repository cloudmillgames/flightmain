// Class: Emitter
// Emits bullets

var Emitter = function() {
    Entity.call(this);
}

Emitter.prototype = new Entity();
Emitter.prototype = {
    constructor: Emitter,
    // Properties
    position: null,
    pemitter: null,
    // Methods
    create: function(_projectilesGroup, _parentSprite, _x, _y) {
        this.parent = _parentSprite;
        this.projectilesGroup = _projectilesGroup;
        this.sprite = game.make.sprite(0, 0, 'position', null);
        this.sprite.anchor = new Phaser.Point(0.5);
        this.position = new Phaser.Point(_x !== undefined? _x : 0, _y !== undefined? _y : 0);
        if(this.parent !== undefined && this.parent != null) {
            this.parent.addChild(this.sprite);
        } else {
            gamesouls.gameLayer.addChild(this.sprite);
        }
        this.sprite.position = this.position;
        // Particles
        this.pemitter = game.make.emitter(this.sprite.worldPosition.x, this.sprite.worldPosition.y, 1000);
        this.pemitter.makeParticles('bullet');
        this.pemitter.setXSpeed(900, 900);
        this.pemitter.setYSpeed(0, 0);
        this.pemitter.setRotation(1, 1);
        this.pemitter.gravity = 0;
        this.pemitter.lifespan = 3500;
        this.projectilesGroup.addChild(this.pemitter);
    },
    update: function() {        
        this.pemitter.emitX = this.sprite.worldPosition.x + 4;
        this.pemitter.emitY = this.sprite.worldPosition.y + 7;
    },
    emit: function(_bullet_class) {
        //if(_bullet_class === undefined) {
        //    _bullet_class = Bullet;
        //}
        //var b = new _bullet_class();
        //b.create(this.projectilesGroup, this.sprite.worldPosition);
        this.pemitter.emitParticle();
    },
};