// Class StartGame
/* global game */
/* global debugsy */
/* global gfxDraw */
/* global gamesouls */

var StartGame = function() {
    
};
StartGame.prototype = {
    init: function() {
        this.loaded = false;
        this.startDelay = 1.0;
    },
    
    preload: function() {
        /*var l = this.add.sprite(game.world.centerX, game.world.centerY - 45, 'loading');
        l.anchor.set(0.5);
        l.animations.add('loading', null, 30, true);
        l.play('loading');*/
        
        
        
        var t = this.add.text(game.world.centerX, game.world.centerY + 50, "LOADING", 
            {fontSize:'32px', font:'lucida console', fill:'#ffffff', align:'center'});
        t.anchor.set(0.5);
        
        this.load.crossOrigin = 'anonymous';
        
        this.load.image('fighter', 'asset/bmp/fighter.png');
        this.load.image('enemy', 'asset/bmp/enemy.png');
        this.load.image('bullet', 'asset/bmp/bullet.png');
        this.load.image('fighter_intro', 'asset/bmp/fighter_intro.png');
        this.load.image('bg_layer_1', 'asset/bmp/bg_layer_1.fw.png');
        this.load.image('bg_layer_2', 'asset/bmp/bg_layer_2.fw.png');
        this.load.image('bg_layer_3', 'asset/bmp/bg_layer_3.fw.png');
        this.load.image('position', 'asset/position.fw.png');
        this.load.image('placeholder', 'asset/placeholder.fw.png');
        this.load.spritesheet('player', 'asset/bmp/flight.fw.png', 51, 22, 3);
        
        this.load.onLoadComplete.add(this.onLoadComplete, this);
        
        gamesouls.LoadWebFont("M20_SP-RANKER", "asset/fonts/M20_SP-RANKER/styles.css");
    },
    
    create: function() {
        gamesouls.outro();
        gfxDraw = game.add.graphics(0, 0);
    },
    
    update: function() {
        if(this.loaded) {
            this.startDelay -= game.time.physicsElapsed;
            if(this.startDelay <= 0.0) {
                game.state.start('MainMenu', true);
            }
        }
    },
    
    render: function() {
        
    },
    
    onLoadComplete: function() {
        this.loaded = true;
        debugsy.log("All Data Loaded");
    },
};
