// Class: Player
// Represents the player ship
var Player = function() {
    Entity.call(this);
}
Player.prototype = new Entity();
Player.prototype = {
    // Constructor
    constructor: Player,
    // Properties
    movementSpeed: 300,
    horizontalEdge: 500,
    emitter: null,
    projectilesGroup: null,
    // Methods
    create: function() {
        this.sprite = game.add.sprite(0, 0, 'player', null, gamesouls.gameLayer);
        this.sprite.anchor = new Phaser.Point(0.5);
        game.physics.arcade.enable(this.sprite);
        this.sprite.body.setSize(51, 22, 0, 0);
        this.sprite.body.collideWorldBounds = true;
        this.sprite.animations.add('idle', [1, 2], 10, true);
        this.sprite.x = 25 + 51;
        this.sprite.y = gamesouls.centerY;
        this.sprite.scale = new Phaser.Point(2, 2);
        this.sprite.play('idle');
        this.projectilesGroup = game.add.group(gamesouls.gameLayer, 'player_projectiles', true, false); 
        this.projectilesGroup.mask = gamesouls.gameLayer.mask;
        //
        this.emitter = new Emitter();
        this.emitter.create(this.projectilesGroup, this.sprite, 23, 9);
    },
    update: function() {
        this.sprite.body.velocity = new Phaser.Point(gamesouls.inputState.horizontal * this.movementSpeed,
                                                     gamesouls.inputState.vertical * this.movementSpeed);
        // Limit horizontal movement back a little
        if(this.sprite.x >= this.horizontalEdge) {
            this.sprite.x = this.horizontalEdge;
        }
        this.emitter.update();
        if(gamesouls.inputState.buttonA_hit) {
            this.emitter.emit();
        }
    },
};