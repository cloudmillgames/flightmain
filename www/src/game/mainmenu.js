// Class MainMenu

/* global game */
/* global debugsy */
/* global DEBUG */
/* global gamesouls */
/* global Phaser */

var MainMenu = function() {
    this.title = null;
    this.startMenu = null;
    this.exitMenu = null;
    this.cursor = null;
    
    this.rtweens = [];
    this.introTimer = null;
    this.introTimerEvent = null;
    this.introFighter = null;
    
    this.START = 'start';
    this.EXIT = 'exit';
    
    this.choice = this.START;
};
MainMenu.prototype = {
    init: function() {
        debugsy.log("Main Menu Scene");
    },
    
    preload: function() {
        
    },
    
    create: function() {
        // Visuals
        var title_font =  {fontSize:'84px', font:'lucida console', fill:'#ffffff', align:'center'};
        this.title = this.add.text(gamesouls.centerX, gamesouls.height + 100, "FLIGHT", 
            title_font, gamesouls.group);
        this.title.anchor.set(0.5);
        var t_in = game.add.tween(this.title);
        t_in.to({y:gamesouls.centerY - 50}, 3000, null);
        t_in.start();
        this.rtweens.push(t_in);
        
        var menu_font = {font:'24px lucida console', fill:'#ffffff', align:'left'};
        
        this.startMenu = this.add.text(gamesouls.centerX - 70, gamesouls.centerY + 40, "Start Game",
            menu_font, gamesouls.group);
        this.startMenu.alpha = 0.0;
        var t1 = game.add.tween(this.startMenu);
        t1.to({alpha:1.0}, 250, null, true, 3250);
        this.rtweens.push(t1);
        
        this.exitMenu = this.add.text(gamesouls.centerX - 70, gamesouls.centerY + 80, "Exit Game",
                menu_font, gamesouls.group);
        this.exitMenu.alpha = 0.0;
        var t2 = game.add.tween(this.exitMenu);
        t2.to({alpha:1.0}, 250, null, true, 3350);
        this.rtweens.push(t2);
        
        /*if(!window.mobileAndTabletcheck()) {        
            var t2 = game.add.tween(this.exitMenu);
            t2.to({alpha:1.0}, 250, null, true, 3350);
            this.rtweens.push(t2);
        } else {
            this.exitMenu.visible = false;
        }*/
        
        this.cursor = this.add.text(gamesouls.centerX - 100, this.startMenu.y, ">",
            menu_font, gamesouls.group);
        this.cursor.alpha = 0.0;
        var t3 = game.add.tween(this.cursor);
        t3.to({alpha:1.0}, 100, null, true, 3450);
        this.rtweens.push(t3);
        
        this.introTimer = game.time.create();
        this.introTimerEvent = this.introTimer.add(3000, this.onIntroTimerComplete, this);
        this.introTimer.start(0);
        
        gamesouls.resetInput();
    },
    
    introFlight: function() {
        this.introFighter = this.add.sprite(0, 0, 'fighter_intro', null, gamesouls.group);
        this.introFighter.x =  -this.introFighter.width;
        this.introFighter.y = gamesouls.centerY;
        var t = game.add.tween(this.introFighter);
        t.to({x:gamesouls.width}, 500, null, true);
    },
    
    update: function() {
        gamesouls.update();
        
        this.fireInput = gamesouls.inputState.buttonA_hit || gamesouls.inputState.buttonSTART_hit;
        this.interruptInput = this.fireInput || gamesouls.inputState.buttonB_hit || gamesouls.inputState.buttonSELECT_hit;
        if(this.introTimerEvent && this.interruptInput) {
            this.introInterrupt();
        } else {
            if(!this.fireInput) {
                //if(!window.mobileAndTabletcheck()) {
                    if(gamesouls.inputState.vertical < -0.5) {
                        this.cursor.y = this.startMenu.y;
                        this.choice = this.START;
                    } else if(gamesouls.inputState.vertical > 0.5) {
                        this.cursor.y = this.exitMenu.y;
                        this.choice = this.EXIT;
                    }
                //}
            } else {
                switch(this.choice) {
                    case this.START:
                        this.startGame();
                        break;
                        
                    case this.EXIT:
                        this.exitGame();
                        break;
                }
            }
        }
    },
    
    render: function() {
        //gfxDraw.clear();
        debugsy.render();
    },
    
    onIntroTimerComplete: function() {
        if(this.introTimerEvent) {
            this.introTimerEvent = null;
        }
        this.introFlight();
    },
    
    introInterrupt: function() {
        for(var i = 0; i < this.rtweens.length; ++i) {
            if(this.rtweens[i].isRunning) {
                this.rtweens[i].stop(true)
            }
        }
        this.onIntroTimerComplete();
        
        // This should run automatically?
        this.title.y = gamesouls.centerY - 50;
        this.startMenu.alpha = 1.0;
        this.exitMenu.alpha = 1.0;
        this.cursor.alpha = 1.0;
    },
    
    startGame: function() {
        debugsy.log("START GAME");
        gamesouls.clearWorld();
        game.state.start('Gameplay', true);
    },
    
    exitGame: function() {
        // do something that counts as exit, perhaps back to home page?
        //window.location.href = 'http://www.google.com/';
        debugsy.log("EXIT GAME");
        if(window.mobileAndTabletcheck()) {
            navigator.app.exitApp();
        } else {
            console.log("History Length: " + window.history.length);
            /*if(window.history.length > 2) {
                console.log("Backing through history..");
                window.history.back();
            } else {*/
                console.log("Redirecting to home..");
                //window.location.href = 'https://www.google.com/';
                if(typeof(window.home) == 'function') {
                    window.home();
                } else if(document.all) { // for stupid IE
                    window.location.href = "about:home";
                } else {
                    window.location.href = "about:home";
                    //window.location.href = "https://www.google.com/";
                }
            //}
        }
    },
};
