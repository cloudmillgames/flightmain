// Class GameSouls

/* global game */
/* global Utils */
/* global debugsy */
/* global PIXI */
/* global Phaser */

var g_mobileDoubleBackTime = 1000;
var g_mobileExitDialog = false;
var g_browserFullscreen = false;

var Layers = {
    UNDERLAY: 0,
    BG: 100,
    GAME: 200,
    HUD: 300,
    OVERLAY: 1000,
};

var WebFontConfig = {
    active: function() { game.time.events.add(Phaser.Timer.SECOND, WebFontReady, this); },
    google: {
        families: ['VT323']
    }
};
function WebFontReady() {
    console.log("Web Font Ready");
}

var GameSouls = function() {
    this.x = 132;
    this.y = 56;
    this.width = 640;
    this.height = 400;
    this.centerX = this.width / 2;
    this.centerY = this.height / 2;
    this.consoleGroup = null;
    this.group = null;
    this.group2 = null;
    this.consoleAesthetic = null;
    this.startingText = null;
    this.fpsRetroText = null;
    this.introDone = false;
    this.gsTitle = null;
    this.axisArea = new Phaser.Rectangle(13, 200, 100, 102);
    this.axisSize = new Phaser.Point(69, 70);
    this.axisSprite = null;
    this.buttonAPos = new Phaser.Point(836, 331);
    this.buttonBPos = new Phaser.Point(836, 177);
    this.buttonSTARTPos = new Phaser.Point(550, 480);
    this.buttonSELECTPos = new Phaser.Point(352, 480);
    this.buttonFSPos = new Phaser.Point(451, 480);
    this.buttonASprite = null;
    this.buttonBSprite = null;
    this.buttonSTARTSprite = null;
    this.buttonSELECTSprite = null;
    this.buttonFSSprite = null;
    // Control
    this.mobileBackFirstPress = false;
    this.mobileBackStartTime = 0;
    // Touch axis
    this.tapArea = new Phaser.Rectangle(0, 154, 181, 195);
    this.tapUp = new Phaser.Rectangle(28, 154, 69, 67);
    this.tapDown = new Phaser.Rectangle(29, 282, 69, 67);
    this.tapLeft = new Phaser.Rectangle(0, 221, 47, 62);
    this.tapRight = new Phaser.Rectangle(80, 221, 101, 62);
    // Layers
    this.layers = {};
};
GameSouls.prototype = {
    // Pre-initialize
    init: function() {
        
    },
    ////////////////////////////// WebFont
    InitWebFontLib: function() {
        //game.load.script('webfont',  'https://ajax.googleapis.com/ajax/libs/webfont/1/webfont.js');
    },
    LoadGoogleWebFont: function(google_font_names) {
        // Fonts need at least 1 second before they can be used
        if(!WebFont || !WebFont.load) {
            console.log("[ERROR] WebFont is not loaded correctly");
            return;
        }
        var fnames = null;
        if(typeof(google_font_names) === "string") {
            fnames = [google_font_names];
        } else if(typeof(google_Font_names) === "object") {
            fnames = google_font_names;
        }
        if(!fnames) {
            console.log("[ERROR] gamesouls.LoadGoogleWebFont is given invalid font names: " + google_font_names);
            return;
        }
        // Loading fonts
        WebFont.load({ 
            google: {
                families: fnames
            }
        });
        for(var n in fnames) {
            console.log("Loaded google font: " + fnames[n]);
        }
    },
    LoadWebFont: function(font_names, css_urls) {
        // Use this tool to create webfonts:
        // https://www.fontsquirrel.com/tools/webfont-generator 
        // Fonts need at least 1 second before they can be used
        if(!WebFont || !WebFont.load) {
            console.log("[ERROR] WebFont is not loaded correctly");
            return;
        }
        var fnames = null;
        var urls = null;
        if(typeof(font_names) === "string") {
            fnames = [font_names];
        } else if(typeof(font_names) === "object") {
            fnames = font_names;
        }
        if(typeof(css_urls) === "string") {
            urls = [css_urls];
        } else if(typeof(css_urls) === "object") {
            urls = css_urls;
        }
        if(!fnames || !urls) {
            console.log("[ERROR] gamesouls.LoadWebFont is given invalid font names or css url: " + font_names + " (" + css_urls + ")");
            return;
        }
        // Loading fonts
        WebFont.load({
            custom: {
                families: fnames,
                urls: urls
            }
        });
        for(var n in fnames) {
            console.log("Loaded custom fonts: " + fnames[n] + " from: " + urls[n]);
        }
    },
    //////////////////////////////////////// Gamesouls virtual console
    create: function() {
        this.InitWebFontLib();
        this.consoleGroup = game.add.group(undefined, 'gamesouls', true); // auto-added to world when undefined
        this.consoleAesthetic = game.add.image(0, 0, 'gamesouls_console', null, this.consoleGroup);         
        // Gamesouls fps
        this.fpsRetroText = game.add.retroFont('gamesouls_fpsretrofont', 49, 64, '0123456789abcdef', 8, 0, 0, 0, 0);
        this.fpsRetroText.setText('00');
        this.fpsSprite = game.add.image(772 - Math.floor(64 * 2 * 0.35), 20, this.fpsRetroText, null, this.consoleGroup);
        this.fpsSprite.scale.x = 0.35;
        this.fpsSprite.scale.y = 0.35;
        // Gamesouls axis
        this.axisSprite = game.add.image(this.axisArea.centerX, this.axisArea.centerY, "gamesouls_axis", null, this.consoleGroup);
        this.axisSprite.anchor.x = 0.5;
        this.axisSprite.anchor.y = 0.5;
        this.virtualAxisArea = new Phaser.Rectangle(0, 56, 312, 400);
        // Gamesouls buttons
        // -- Create sprite
        this.buttonASprite = game.add.sprite(this.buttonAPos.x, this.buttonAPos.y, "gamesouls_buttons", 0, this.consoleGroup);
        this.buttonBSprite = game.add.sprite(this.buttonBPos.x, this.buttonBPos.y, "gamesouls_buttons", 2, this.consoleGroup); 
        this.buttonSTARTSprite = game.add.sprite(this.buttonSTARTPos.x, this.buttonSTARTPos.y, "gamesouls_buttons2", 2, this.consoleGroup);
        this.buttonSELECTSprite = game.add.sprite(this.buttonSELECTPos.x, this.buttonSELECTPos.y, "gamesouls_buttons2", 0, this.consoleGroup);
        // -- Set anchor
        this.buttonASprite.anchor.set(0.5);
        this.buttonBSprite.anchor.set(0.5);
        this.buttonSTARTSprite.anchor.set(0.5);
        this.buttonSELECTSprite.anchor.set(0.5);
        // -- enable input
        this.buttonASprite.inputEnabled = true;
        this.buttonBSprite.inputEnabled = true;
        this.buttonSTARTSprite.inputEnabled = true;
        this.buttonSELECTSprite.inputEnabled = true;
        // -- Desktop fullscreen button
        //if(!window.mobileAndTabletcheck()) {
            // No need, using HTML5 button instead
            //this.buttonFSSprite = game.add.sprite(this.buttonFSPos.x, this.buttonFSPos.y, "gamesouls_buttons3", 0, this.consoleGroup);
            //this.buttonFSSprite.anchor.set(0.5);
            //this.buttonFSSprite.inputEnabled = true;
        //}
        // Play LCD
        this.group = game.add.group(this.consoleGroup, 'gamesouls', true);
        this.group.x = 132;
        this.group.y = 56;
        this.group.mask = new PIXI.Graphics();
        this.group.mask.isMask = true;
        this.group.mask.beginFill(0xffffff, 1.0);
        this.group.mask.drawRect(132, 56, 640, 400);
        this.group.mask.endFill();
        game.world.bounds = new Phaser.Rectangle(this.x, this.y, this.width, this.height);
        // -- Standard layers
        this._createDefaultLayers();
        // Second screen
        this.group2 = game.add.group(this.consoleGroup, 'second_screen', true);
        this.group2.x = 252;
        this.group2.y = 9;
        this.group2.mask = new PIXI.Graphics();
        this.group2.mask.isMask = true;
        this.group2.mask.beginFill(0xffffff, 1.0);
        this.group2.mask.drawRect(252, 9, 400, 36);
        this.group2.mask.endFill();
        // Input system
        this._initInput();
        // Debugsy
        debugsy.create();
        if(!window.cordova) {
            if(window.mobileAndTabletcheck()) {
                debugsy.log("Welcome! (Mobile)");
                // Attempt to hide address bar
                window.scrollTo(0,1);
            } else {
                debugsy.log("Welcome! (PC Browser)");
            }            
            // Write fullscreen button:
            document.getElementById("fullscreen_btn").innerHTML += 
                '<input type="image" src="asset/gamesouls/fullscreen_btn.fw.png" name="goFullscreen" class="btTxt fullscreen" id="goFullscreen" style="position: absolute; right: 5px; top: 5px;" />';
            var goFS = document.getElementById("goFullscreen");
            goFS.addEventListener("click", function() { Utils.ToggleFullscreen(); }, false);
        } else {
            debugsy.log("Welcome! (Cordova)");
            // Setup special mobile handlers
            document.addEventListener('pause', this._onMobilePause, false);
            document.addEventListener('resume', this._onMobileResume, false);
            document.addEventListener('backbutton', this._onMobileBack, false);
            document.addEventListener('menubutton', this._onMobileMenu, false);
            // only iOS: when device is locked and app in foreground, resign evnet
            if(device.platform === 'iOS') {
                document.addEventListener('resign', this._oniOSResign, false);
            }
        }
    },
    intro: function() {
        var title = game.add.image(this.centerX, this.height, 'gamesouls_title', null, this.group);
        title.anchor.x = 0.5;
        title.anchor.y = 0.5;
        this.gsTitle = title;
        var tw = game.add.tween(title);
        tw.to({y:this.centerY - 25}, 750, null, true, 0);
        tw.onComplete.add(this._onTitleIn, this);
    },
    _onTitleIn: function() {
        var starting = game.add.image(this.centerX, this.centerY + 25, 'gamesouls_starting', null, this.group);
        starting.anchor.x = 0.5;
        starting.anchor.y = 0.5;
        starting.visible = true;
        this.startingText = starting;
        
        var tw = game.add.tween(this.startingText);
        var t = tw.to({y:this.startingText.y}, 750, null, false, 0, 0);
        t.repeat(-1);
        t.start();
        t.onLoop.add(this._onStartingBlink, this, 0);
        //t.onLoop.add((function() { console.log("BLINK 2?"); this.startingText.visible = !this.startingText.visible}), this, 0);
        
        this.introDone = true;
    },
    _onStartingBlink: function(_object, _tween) {
        _object.visible = !_object.visible;
    },
    update: function() {
        this._updateFPS();
        this._updateInput();
    },
    drawDebug: function() {
        //game.debug.geom(Phaser.Rectangle, "#808080", true);
        //game.debug.pointer()
        this.consoleAesthetic.alpha = 0.2;
    },
    outro: function() {
        this.gsTitle.destroy();
        this.startingText.destroy();
    },
    isIntroDone: function() {
        return this.introDone;  
    },
    clearWorld: function() {
        this.group.removeAll(true, false);
    },
    _updateFPS : function() {
        if(game.time.advancedTiming) {
            var o = game.time.fps.toString().substr(0, 2);
            if(o.length == 1) {
                o = "0" + o;
            }
            this.fpsRetroText.setText(o);
        }
    },
    ////////////////////////////////////////////// Gamesouls Layers
    _createDefaultLayers: function() { 
        this.underLayer = this.createLayer(Layers.UNDERLAY, this.group, "UNDERLAY");
        this.bgLayer = this.createLayer(Layers.BG, this.group, "BG");
        this.gameLayer = this.createLayer(Layers.GAME, this.group, "GAME");
        this.hudLayer = this.createLayer(Layers.HUD, this.group, "HUD");
        this.overLayer = this.createLayer(Layers.OVERLAY, this.group, "OVERLAY");
    },
    createLayer: function(id, parent, name) {
        if(!id) {
            console.log("[ERROR] gamesouls.addLayer() given an invalid id: ", id);
        }
        if(this.layers.hasOwnProperty(id)) {
            console.log("[ERROR] gamesouls.addLayer() given an id that doesn't exist: " + id);
        }
        if(!parent) {
            parent = this.group;
        }
        if(!name) {
            name = "layer";
        }
        this.layers[id] = game.add.group(parent, name, true);
        this.layers[id].x = parent.x;
        this.layers[id].y = parent.y;
        this.layers[id].mask = parent.mask;
        console.log("Gamesouls created layer: " + name);
        return this.layers[id];
    },
    getLayer: function(id) {
        // No guard check to save some time because this call is repetitive
        return this.layers[id];
    },
    ////////////////////////////////////////////// Gamesouls Input
    resetInput: function() {
       this._initInput();
    },
    _initInput: function() {
        // - Keyboard
        game.input.keyboard.addCallbacks(this, this._onKeyboardDown, this._onKeyboardUp);
        // - Touch
        game.input.maxPointers = 4;
        // -- setup callback events per button
        this.buttonASprite.events.onInputDown.add(this._onButtonADown, this); 
        this.buttonASprite.events.onInputUp.add(this._onButtonAUp, this);
        this.buttonBSprite.events.onInputDown.add(this._onButtonBDown, this); 
        this.buttonBSprite.events.onInputUp.add(this._onButtonBUp, this);
        this.buttonSTARTSprite.events.onInputDown.add(this._onButtonSTARTDown, this); 
        this.buttonSTARTSprite.events.onInputUp.add(this._onButtonSTARTUp, this);
        this.buttonSELECTSprite.events.onInputDown.add(this._onButtonSELECTDown, this); 
        this.buttonSELECTSprite.events.onInputUp.add(this._onButtonSELECTUp, this);
        if(!window.mobileAndTabletcheck()) {
            //this.buttonFSSprite.events.onInputDown.add(this._onButtonFSDown, this);
            //this.buttonFSSprite.events.onInputUp.add(this._onButtonFSUp, this);
        }
        // - Gamepad
        game.input.gamepad.start();
        this.pad1 = game.input.gamepad.pad1;
        if(this.pad1) {
            this.pad1.addCallbacks(this, {
                onConnect: this._onGamepadConnect,
                onDisconnect: this._onGamepadDisconnect,
                onDown: this._onGamepadDown,
                onUp: this._onGamepadUp,
                onAxis: this._onGamepadAxis
            });
        }
        // Controls
        this._inputData = {
            left: 0.0,
            right: 0.0,
            up: 0.0,
            down: 0.0,
            buttonA: false,
            buttonB: false,
            buttonSELECT: false,
            buttonSTART: false,
            buttonFS: false,
        };
        this.inputState = {
            horizontal: 0.0,
            vertical: 0.0,
            buttonA: false,
            buttonB: false,
            buttonSELECT: false,
            buttonSTART: false,
            buttonFS: false,
            buttonA_hit: false,
            buttonA_release: false,
            buttonB_hit: false,
            buttonB_release: false,
            buttonSTART_hit: false,
            buttonSTART_release: false,
            buttonSELECT_hit: false,
            buttonSELECT_release: false,
            buttonFS_hit: false,
            buttonFS_release: false,
        };
        this.isVirtualAxisActive = false;
        this.virtualAxisPointer = null;
        this.virtualAxisStartPoint = null;
        this.touchEventsRegistered = false;
    },
    _updateInput: function() {
        if(!this.touchEventsRegistered) {
            // Touch
            game.input.onDown.add(this._onTouchDown, this);
            //game.input.onTap.add(this._onTap, this);
            game.input.onUp.add(this._onTouchUp, this);
            this.touchEventsRegistered = true;
        } else if(this.isVirtualAxisActive) {
            var max_mag = 50;
            var x_ratio = Utils.Clamp(this.virtualAxisPointer.x - this.virtualAxisStartPoint.x, -max_mag, max_mag) / max_mag;
            var y_ratio = Utils.Clamp(this.virtualAxisPointer.y - this.virtualAxisStartPoint.y, -max_mag, max_mag) / max_mag;
            if(x_ratio > 0.0) {
                this._inputData.right = x_ratio;
                this._inputData.left = 0.0;
            } else {
                this._inputData.right = 0.0;
                this._inputData.left = x_ratio;
            }
            if(y_ratio > 0.0) {
                this._inputData.down = y_ratio;
                this._inputData.up = 0.0;
            } else {
                this._inputData.down = 0.0;
                this._inputData.up = y_ratio;
            }
        }
        // Movement
        this.inputState.horizontal = Utils.Clamp(this._inputData.left + this._inputData.right, -1.0, 1.0);
        this.inputState.vertical = Utils.Clamp(this._inputData.up + this._inputData.down, -1.0, 1.0);
        // Hit
        this.inputState.buttonA_hit = !this.inputState.buttonA && this._inputData.buttonA;
        this.inputState.buttonB_hit = !this.inputState.buttonB && this._inputData.buttonB;
        this.inputState.buttonSTART_hit = !this.inputState.buttonSTART && this._inputData.buttonSTART;
        this.inputState.buttonSELECT_hit = !this.inputState.buttonSELECT && this._inputData.buttonSELECT;
        this.inputState.buttonFS_hit = !this.inputState.buttonFS && this._inputData.buttonFS;
        // Release
        this.inputState.buttonA_release = this.inputState.buttonA && !this._inputData.buttonA;
        this.inputState.buttonB_release = this.inputState.buttonB && !this._inputData.buttonB;
        this.inputState.buttonSTART_release = this.inputState.buttonSTART && !this._inputData.buttonSTART;
        this.inputState.buttonSELECT_release = this.inputState.buttonSELECT && !this._inputData.buttonSELECT;
        this.inputState.buttonFS_release = this.inputState.buttonFS && this._inputData.buttonFS;
        // Down
        this.inputState.buttonA = this._inputData.buttonA;
        this.inputState.buttonB = this._inputData.buttonB;
        this.inputState.buttonSTART = this._inputData.buttonSTART;
        this.inputState.buttonSELECT = this._inputData.buttonSELECT;
        this.inputState.buttonFS = this._inputData.buttonFS;
        this._moveAxisStick(this.inputState.horizontal, this.inputState.vertical);
        this._hitButtons(this.inputState.buttonA, this.inputState.buttonB, this.inputState.buttonSTART, this.inputState.buttonSELECT,           
                         this.inputState.buttonFS);
        
        if(!window.cordova && window.mobileAndTabletcheck()) {
            if(this.inputState.buttonFS_hit) {
                // ERROR: Request for full-screen was denied because Element.mozRequestFullScreen() was not 
                //        called from inside a short running user-generated event handler.
                // Utils.ToggleFullscreen();
                // g_browserFullscreen = !g_browserFullscreen;
            }
        }
    },
    _moveAxisStick: function(horizontal, vertical) {
        horizontal = Utils.Clamp(horizontal, -1.0, 1.0);
        vertical = Utils.Clamp(vertical, -1.0, 1.0);
        this.axisSprite.x = this.axisArea.centerX + (horizontal * (this.axisSize.x / 4));
        this.axisSprite.y = this.axisArea.centerY + (vertical * (this.axisSize.y / 4));
    },
    _hitButtons: function(a_down, b_down, start_down, select_down, fs_down) {
        this.buttonASprite.frame = a_down? 1 : 0;
        this.buttonBSprite.frame = b_down? 3 : 2;
        this.buttonSTARTSprite.frame = start_down? 3 : 2;
        this.buttonSELECTSprite.frame = select_down? 1 : 0;
        if(!window.mobileAndTabletcheck()) {
            //this.buttonFSSprite.frame = fs_down? 1 : 0;
        }
    },
    // Keyboard input events
    _onKeyboardDown: function(e) {
        switch(e.keyCode) {
            case Phaser.KeyCode.RIGHT:
                this._inputData.right = 1.0;
                break;
            case Phaser.KeyCode.LEFT:
                this._inputData.left = -1.0;
                break;
            case Phaser.KeyCode.UP:
                this._inputData.up = -1.0;
                break;
            case Phaser.KeyCode.DOWN:
                this._inputData.down = 1.0;
                break;
            case Phaser.KeyCode.CONTROL:
            case Phaser.KeyCode.Z:
                this._inputData.buttonA = true;
                break;
            case Phaser.KeyCode.SHIFT:
            case Phaser.KeyCode.X:
                this._inputData.buttonB = true;
                break;
            case Phaser.KeyCode.SPACEBAR:
                this._inputData.buttonSELECT = true;
                break;
            case Phaser.KeyCode.ENTER:
                this._inputData.buttonSTART = true;
                break;
        }
    },
    _onKeyboardUp: function(e) {
        switch(e.keyCode) {
            case Phaser.KeyCode.RIGHT:
                this._inputData.right = 0.0;
                break;
            case Phaser.KeyCode.LEFT:
                this._inputData.left = 0.0;
                break;
            case Phaser.KeyCode.UP:
                this._inputData.up = 0.0;
                break;
            case Phaser.KeyCode.DOWN:
                this._inputData.down = 0.0;
                break;
            case Phaser.KeyCode.CONTROL:
            case Phaser.KeyCode.Z:
                this._inputData.buttonA = false;
                break;
            case Phaser.KeyCode.SHIFT:
            case Phaser.KeyCode.X:
                this._inputData.buttonB = false;
                break;
            case Phaser.KeyCode.SPACEBAR:
                this._inputData.buttonSELECT = false;
                break;
            case Phaser.KeyCode.ENTER:
                this._inputData.buttonSTART = false;
                break;
        }
    },
    // event is MouseEvent or Touch
    _onTouchDown: function(pointer, event) {
        if(pointer && event) {
            //if(Phaser.Rectangle.contains(this.virtualAxisArea, event.clientX, event.clientY)) {
            if(Phaser.Rectangle.contains(this.virtualAxisArea, pointer.x, pointer.y)) {
                this.isVirtualAxisActive = true;
                this.virtualAxisPointer = pointer;
                this.virtualAxisStartPoint = new Phaser.Point(63, 251); // Hard coded center of axis
            }
        }
    },
    _onTouchUp: function(pointer, event) {
        if(this.isVirtualAxisActive && event && pointer === this.virtualAxisPointer) {
            this.isVirtualAxisActive = false;
            this.virtualAxisPointer = null;
            this.virtualAxisStartPoint = null;
            this._inputData.left = 0.0;
            this._inputData.right = 0.0;
            this._inputData.up = 0.0;
            this._inputData.down = 0.0;
        }
    },
    // Virtual Gamepad input events
    _onButtonADown: function() {
        this._inputData.buttonA = true;
    },
    _onButtonAUp: function() {
        this._inputData.buttonA = false;
    },
    _onButtonBDown: function() {
        this._inputData.buttonB = true;
    },
    _onButtonBUp: function() {
        this._inputData.buttonB = false;
    },
    _onButtonSTARTDown: function() {
        this._inputData.buttonSTART = true;
    },
    _onButtonSTARTUp: function() {
        this._inputData.buttonSTART = false;
    },
    _onButtonSELECTDown: function() {
        this._inputData.buttonSELECT = true;
    },
    _onButtonSELECTUp: function() {
        this._inputData.buttonSELECT = false;
    },
    _onButtonFSDown: function() {
        this._inputData.buttonFS = true;
    },
    _onButtonFSUp: function() {
        this._inputData.buttonFS = false;
    },
    _logConnectedPads: function() {
        console.log("Gamepads connected: " + game.input.gamepad.padsConnected.toString());
        if(game.input.gamepad.pad1 && game.input.gamepad.pad1.connected) {
            console.log("PAD 1");
        } 
        if(game.input.gamepad.pad2 && game.input.gamepad.pad2.connected) {
            console.log("PAD 2");
        }
        if(game.input.gamepad.pad3 && game.input.gamepad.pad3.connected) {
            console.log("PAD 3");
        }
        if(game.input.gamepad.pad4 && game.input.gamepad.pad4.connected) {
            console.log("PAD 4");
        }  
    },
    // Gamepad input events
    _onGamepadConnect: function() {
        debugsy.log("Gamepad connected");
        this._logConnectedPads();
    },
    _onGamepadDisconnect: function() {
        debugsy.log("Gamepad disconnected");
        this._logConnectedPads();
    },
    _onGamepadDown: function(btn_idx) {        
        if(!this.pad1) {
            debugsy.log("Gamepad undefined");
            this._logConnectedPads();
            return;
        }
        switch(btn_idx) {
            case Phaser.Gamepad.BUTTON_0: // A
                this._inputData.buttonA = true;
                break;
            case Phaser.Gamepad.BUTTON_1: // B
                this._inputData.buttonB = true;
                break;
            case Phaser.Gamepad.BUTTON_8: // SELECT/BACK
                this._inputData.buttonSELECT = true;
                break;
            case Phaser.Gamepad.BUTTON_9: // START
                this._inputData.buttonSTART = true;
                break;
            // DPAD for XBOX360 and PS3
            case Phaser.Gamepad.XBOX360_DPAD_LEFT:
                this._inputData.left = -1.0;
                break;
            case Phaser.Gamepad.XBOX360_DPAD_RIGHT:
                this._inputData.right = 1.0;
                break;
            case Phaser.Gamepad.XBOX360_DPAD_UP:
                this._inputData.up = -1.0;
                break;
            case Phaser.Gamepad.XBOX360_DPAD_DOWN:
                this._inputData.down = 1.0;
                break;
        }
    },
    _onGamepadUp: function(btn_idx) {
        if(!this.pad1) {
            debugsy.log("Gamepad undefined");
            this._logConnectedPads();
            return;
        }
        switch(btn_idx) {
            case Phaser.Gamepad.BUTTON_0: // A
                this._inputData.buttonA = false;
                break;
            case Phaser.Gamepad.BUTTON_1: // B
                this._inputData.buttonB = false;
                break;
            case Phaser.Gamepad.BUTTON_8: // SELECT/BACK
                this._inputData.buttonSELECT = false;
                break;
            case Phaser.Gamepad.BUTTON_9: // START
                this._inputData.buttonSTART = false;
                break;
            // DPAD for XBOX360 and PS3
            case Phaser.Gamepad.XBOX360_DPAD_LEFT:
                this._inputData.left = 0.0;
                break;
            case Phaser.Gamepad.XBOX360_DPAD_RIGHT:
                this._inputData.right = 0.0;
                break;
            case Phaser.Gamepad.XBOX360_DPAD_UP:
                this._inputData.up = 0.0;
                break;
            case Phaser.Gamepad.XBOX360_DPAD_DOWN:
                this._inputData.down = 0.0;
                break;
        }
    },
    _onGamepadAxis: function(e, axis_idx) {
        if(!this.pad1) {
            debugsy.log("Gamepad undefined");            
            this._logConnectedPads();
            return;
        }
        var av = this.pad1.axis(axis_idx);
        switch(axis_idx) {
            case Phaser.Gamepad.AXIS_0: // Horizontal
                if(av > 0.0) {
                    this._inputData.right = av;
                    this._inputData.left = 0.0;
                } else {
                    this._inputData.right = 0.0;
                    this._inputData.left = av;
                }
                break;
            case Phaser.Gamepad.AXIS_1: // Vertical
                 if(av > 0.0) {
                     this._inputData.up = 0.0;
                     this._inputData.down = av;
                 } else {
                    this._inputData.up = av;
                    this._inputData.down = 0.0;
                 }
                break;
        }
    },
    ////////////////////////////////////////////////// Mobile Events
    _onMobilePause: function(evt) {
        // Pause: when app goes to background by user or an authority app
        // You literally save game here (use localStorage)
        // only iOS: all interactive calls don't work on pause but after app is brought back (ex: console.log)
        
    },
    _onMobileResume: function(evt) {
        // Resume: when app is resumed from the background
        // You literally load game here (use localStorage)
        // only iOS: when called from a resume functions such as alert() need to be wrapped in a 
        //           setTimeout() call with a timeout value of zero, or else the app hangs.
        debugsy.log("_onMobileResume: App Resume called");
        if(window.plugins && window.plugins.toast) { // Use toast
            window.plugins.toast.showWithOptions(
                {
                    message: "_onMobileResume: App Resume called",
                    duration: "short",
                    position: "top",
                },
                undefined,
                function(b) { alert("Toast error: " + b) }
            );
        } else {
            navigator.notification.alert("_onMobileResume: App Resume called", function(){});
        }
    },
    _onMobileBack: function(evt) {
        // Back: when back button is pressed
        if(window.plugins && window.plugins.toast) { // Use toast
            if(!this.mobileBackFirstPress) {
                debugsy.log("Press Back again to exit game..");
                window.plugins.toast.showWithOptions(
                    {
                        message: "Press Back again to exit game",
                        duration: "short",
                        position: "top",
                    },
                    undefined,
                    function(b) { alert("Toast error: " + b) }
                );
                this.mobileBackTime = game.time.now;
                this.mobileBackFirstPress = true;
            } else {
                if(game.time.now - this.mobileBackTime < g_mobileDoubleBackTime) {
                    navigator.app.exitApp();
                } else {
                    this.mobileBackFirstPress = false;
                    this.mobileBackTime = 0;
                }
            }
        } else { // Use confirmation
            if(!g_mobileExitDialog) {
                g_mobileExitDialog = true;
                navigator.notification.confirm("", 
                    function(btn_index){
                        g_mobileExitDialog = false;
                        if(btn_index == 1) {
                            navigator.app.exitApp();
                        }
                    }, "Exit?", ["Yes", "No"]);
            }
        }
    },
    _onMobileMenu: function(evt) {
        // Menu: when menu button is pressed  
    },
    _oniOSResign: function(evt) {
        // Resign: only iOS, device is locked when app is foreground. iOS 5+ triggers pause event after, older doesn't.
    },
};
