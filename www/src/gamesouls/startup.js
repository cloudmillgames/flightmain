// Class  Startup

/* global Phaser */
/* global game */
/* global gamesouls */

var Startup = function() {
    
};
Startup.prototype = {
    init: function() {
        game.stage.backgroundColor = '#272A29';
        game.renderer.renderSession.roundPixels = true;
        game.stage.smoothed = false;
        game.time.advancedTiming = true;
        if(window.mobileAndTabletcheck()) {
            game.scale.scaleMode = Phaser.ScaleManager.SHOW_ALL;
        } else {
            game.scale.scaleMode = Phaser.ScaleManager.NO_SCALE;
        }
        game.scale.alignPageHorizontally = true;
        game.scale.alignPageVertically = true;
        game.scale.fullScreenScaleMode = Phaser.ScaleManager.SHOW_ALL;
    
        this.loaded = false;
    },
    
    preload: function() {
        this.load.crossOrigin = 'anonymous';
        this.load.image('gamesouls_console', 'asset/gamesouls/gamesouls_concept.fw.png');
        this.load.image('gamesouls_bg', 'asset/gamesouls/gamesouls_bg.fw.png');
        this.load.image('gamesouls_title', 'asset/gamesouls/gamesouls_title.fw.png');
        this.load.image('gamesouls_starting', 'asset/gamesouls/gamesouls_starting.fw.png');
        this.load.image('gamesouls_fpsretrofont', 'asset/gamesouls/gamesouls_framerate_font.png');
        this.load.image('gamesouls_axis', 'asset/gamesouls/gamesouls_axis.fw.png');
        this.load.spritesheet('gamesouls_buttons', 'asset/gamesouls/gamesouls_buttons.fw.png', 111, 111, 4, 0, 0);
        this.load.spritesheet('gamesouls_buttons2', 'asset/gamesouls/gamesouls_buttons2.fw.png', 96, 37, 4, 0, 0);
        this.load.spritesheet('gamesouls_buttons3', 'asset/gamesouls/gamesouls_buttons3.fw.png', 38, 31, 2, 0, 0);
        this.load.onLoadComplete.add(this.onLoadComplete, this);
    },
    
    onLoadComplete: function(e) {
        console.log("Loading sprite ready");
        this.loaded = true;
    },
    
    create: function() {
        gamesouls.create();
        gamesouls.intro();
    },
    
    update: function() {
        if(this.loaded) {
            gamesouls.update();
            if(gamesouls.isIntroDone()) {
                game.state.start("StartGame", false);
            }
        } 
    },
};
