// Class Debugsy
// Debugging bar class (based on CMG's impl)
/* global Utils */
var Debugsy = function(_game) {
    this.game = _game;
};
Debugsy.prototype = {
    create: function() {
        this.message = '';
        this.startMessage = false;
        this.ongoingMessage = false;
        this.displayTimer = 0.0;
        this.fadeTimer = 0.0;
        this.FADE_TIME = 0.25;
    },
    
    render: function() {
        if(!DEBUG) return;
        var alpha = 0.0;
        if(this.startMessage) {
            this.displayTimer = 1.0;
            this.fadeTimer = this.FADE_TIME;
            this.startMessage = false;
            this.ongoingMessage = true;
        }
        if(this.ongoingMessage) {
            if(this.displayTimer > 0.0) {
                this.displayTimer = Math.max(0.0, this.displayTimer - this.game.time.physicsElapsed);
                alpha = 1.0;
            } else if(this.fadeTimer > 0.0) {
                this.fadeTimer = Math.max(0.0, this.fadeTimer - this.game.time.physicsElapsed);
                alpha = Utils.Clamp(this.fadeTimer / this.FADE_TIME, 0.0, 1.0); // 1.0 -> 0.0
            } else {
                alpha = 0.0;
                this.message = '';
                this.ongoingMessage = false;
            }
            this.game.debug.geom(new Phaser.Rectangle(this.game.world.bounds.x, this.game.world.bounds.y + this.game.world.bounds.height - 12, this.game.world.bounds.width, 12), 
                'rgba(27, 2, 44, '+ alpha.toString() + ')');
            if(this.message && this.message.length > 0) {
                this.game.debug.text(this.message, this.game.world.bounds.x, this.game.world.bounds.y + this.game.world.bounds.height - 3, 
                    'rgba(176, 176, 176, ' + alpha.toString() + ')', '9px consolas');
            }
        }
    },
    
    log: function(_message) {
        this.message = _message;
        this.startMessage = true;
        console.log(_message);
    },
};
